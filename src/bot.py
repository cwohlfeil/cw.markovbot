from typing import Any, List
import random
import discord
from discord.ext import commands
import markovify
from re import sub, MULTILINE
from config import CONFIG


"""
Utilities
"""
def store_markov_text(text: str) -> None:
    with open(CONFIG['DATABASE'], 'a') as file:
        file.write(f'{text}\n')


def get_markov_sentence() -> str:
    with open(CONFIG['DATABASE']) as f:
        text = f.read()
    text_model = markovify.Text(text, state_size=2)
    return text_model.make_sentence(tries=100)

def clean_message(message) -> str:
    # Remove links
    cleaned_message = sub(r'^https?:\/\/.*[\r\n]*', '', message, flags=MULTILINE)
    cleaned_message = cleaned_message.strip()  # Remove leading and trailing whitespace 
    if not cleaned_message:
        return None
    return cleaned_message

"""
Bot
"""
bot = commands.Bot(
    command_prefix=commands.when_mentioned_or(CONFIG['PREFIX']),
    description=CONFIG['HELP_TEXT']
)


@bot.event
async def on_ready() -> None:
    print(f'Logged in as: {bot.user.name}, {bot.user.id}')
    await bot.change_presence(activity=discord.Game(name=CONFIG['GAME']))
    for server in bot.servers:
        print(f'In server(s): {server.name}')


@bot.event
async def on_message(message) -> None:
    # ignore the bots own messages
    if message.author == bot.user:
        return

    # When a message is written to a server, get cleaned message, clean it further, store it
    cleaned_message = clean_message(message.clean_content)
    store_markov_text(text) if cleaned_message  # Ternary if cleaned_message is not None
        
    # Give a random chance between [0.0, 1.0] to reply with a markov sentence from stored messages. 
    if random.random() < CONFIG['CHANCE_PERCENTAGE']:
        await bot.say(get_markov_sentence())
    await bot.process_commands(message)

if __name__ == '__main__':
    bot.run(CONFIG['TOKEN'])
