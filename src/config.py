import SQLAlchemy


CONFIG = {
    'OWNER_ID': '',
    'TOKEN': '',
    'NICKNAME': '',
    'GAME': '',
    'DATABASE_URL': f'sqlite:////{os.path.join(basedir, "markovbot.db")}',
    'CHANCE_PERCENTAGE': 0.01,
}

# Create the SqlAlchemy db instance
db = SQLAlchemy(app)