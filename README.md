Just a humble discord bot for markov meme purposes. Markov corpus has no seperation between servers for now.

Adapted from <https://github.com/KoopaKiy/Discord-Markov>. For more info see <https://medium.com/@moomooptas/how-to-make-a-simple-discord-bot-in-python-40ed991468b4>

## TODO

* Support multiple servers (store corpus in database or file instead of in memory)
* Support generating markov for specific users (see above)
* Support explicit message count limits (currently hardcoded in deque to 500)
* Unit tests
* Move config to ini at the top level (currently a dict)
* setup.py
* TTS for the big meme
* better error handling
* request rate limiting